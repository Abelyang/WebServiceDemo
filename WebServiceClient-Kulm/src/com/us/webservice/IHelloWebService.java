
/**
 * IHelloWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */

package com.us.webservice;

/*
 *  IHelloWebService java interface
 */

public interface IHelloWebService {

	/**
	 * Auto generated method signature
	 * 
	 * @param sayHello0
	 */

	public com.us.webservice.SayHelloResponseE sayHello(

	com.us.webservice.SayHelloE sayHello0) throws java.rmi.RemoteException;

	/**
	 * Auto generated method signature for Asynchronous Invocations
	 * 
	 * @param sayHello0
	 */
	public void startsayHello(

	com.us.webservice.SayHelloE sayHello0,

	final com.us.webservice.IHelloWebServiceCallbackHandler callback)

	throws java.rmi.RemoteException;

	//
}

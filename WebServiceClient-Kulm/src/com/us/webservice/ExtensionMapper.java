/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:02:19 GMT)
 */

package com.us.webservice;

/**
 * ExtensionMapper class
 */
@SuppressWarnings({ "unchecked", "unused" })
public class ExtensionMapper {

	public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
			java.lang.String typeName, javax.xml.stream.XMLStreamReader reader)
			throws java.lang.Exception {

		if ("http://webservice.us.com/".equals(namespaceURI)
				&& "sayHello".equals(typeName)) {

			return com.us.webservice.SayHello.Factory.parse(reader);

		}

		if ("http://webservice.us.com/".equals(namespaceURI)
				&& "sayHelloResponse".equals(typeName)) {

			return com.us.webservice.SayHelloResponse.Factory.parse(reader);

		}

		throw new org.apache.axis2.databinding.ADBException("Unsupported type "
				+ namespaceURI + " " + typeName);
	}

}

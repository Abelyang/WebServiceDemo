package com.us.webservice;


import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.us.kulm.config.annotation.Reference;


public class WebServiceClient {


	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:consumer.xml");
		if (context != null) {
			context.start();
			IHelloWebService stub = (IHelloWebService) context
					.getBean("iHelloWebService");
			while (true) {
				Thread.sleep(2000);
				stub = new IHelloWebServiceStub();
				SayHelloE request = new SayHelloE();
				SayHello param = new SayHello();
				param.setArg0("AVDC");
				request.setSayHello(param);
				SayHelloResponseE response = stub.sayHello(request);
				System.out.println(response.getSayHelloResponse().get_return());
			}
		}

	}
}

package com.us.webservice;

import com.us.kulm.config.annotation.Service;

@Service(protocol={"webservice"})
public class HelloWebService implements IHelloWebService{
	
	public String sayHello(String name) {
		System.out.println("调用了");
		return "Hello," + name;
	}
}

package com.us.webservice;

import javax.jws.WebParam;

public interface IHelloWebService {
	public String sayHello(@WebParam(name="name")String name);
}

package com.us.webservice;

public class WebClient {
	public static HelloWebService stub; 
	public static void testCodeClient() {  
	    try {  
//	      String url = "http://127.0.0.1/WebService-Kulm/services/HelloWebService?wsdl";  
	      stub = new HelloWebServiceStub();  
	      SayHello request = new SayHello(); 
	      request.setName("ABCD");
	      SayHelloResponse response = stub.sayHello(request);  
	      System.out.println(response.get_return());  
	    } catch (org.apache.axis2.AxisFault e) {  
	      e.printStackTrace();  
	    } catch (java.rmi.RemoteException e) {  
	      e.printStackTrace();  
	    }  
	  
	  }  
	  public static void main(String[] args) {
		  WebClient.testCodeClient();
	}
}

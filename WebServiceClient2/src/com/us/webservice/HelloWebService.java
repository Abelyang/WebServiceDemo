

/**
 * HelloWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.1  Built on : Feb 20, 2016 (10:01:29 GMT)
 */

    package com.us.webservice;

    /*
     *  HelloWebService java interface
     */

    public interface HelloWebService {
          

        /**
          * Auto generated method signature
          * 
                    * @param sayHello0
                
         */

         
                     public com.us.webservice.SayHelloResponse sayHello(

                        com.us.webservice.SayHello sayHello0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param sayHello0
            
          */
        public void startsayHello(

            com.us.webservice.SayHello sayHello0,

            final com.us.webservice.HelloWebServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    
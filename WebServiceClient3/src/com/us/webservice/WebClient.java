package com.us.webservice;

import org.springframework.context.support.ClassPathXmlApplicationContext;
public class WebClient {

	
	public static void main(String[] args)throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:consumer.xml" });
		context.start();
		IHelloWebService iHelloWebService = (IHelloWebService) context
				.getBean("iHelloWebService");
		while (true) {
			Thread.sleep(3000);
			System.out.println(iHelloWebService.sayHello("ABCD"));
		}
	}
}
